﻿using System.Web;
using System.Web.Mvc;

namespace MerchantPlatform_ECommerce_UserWebAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
